<html>
    <head>
        <meta charset="utf-8" />
        <title>Bootstrap TreeView with Checkboxes</title>
        <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
        <link rel="stylesheet" href="/res/plugins/bootstrap/dist/css/bootstrap.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-treeview/1.2.0/bootstrap-treeview.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-treeview/1.2.0/bootstrap-treeview.min.css" />

    </head>
    <body>
        <div class="container-fluid">
            <div class="row">
                <button id="btnSave" class="btn btn-default">Save Checked Nodes</button>
            </div>
            <div class="row">
                <div id="tree"></div>
            </div>
        </div>
        <script type="text/javascript">
            $(document).ready(function () {
                
            });
        </script>
    </body>
</html>