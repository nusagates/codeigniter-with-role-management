function success(title, message) {
    swal({
        title: title,
        text: message,
        icon: "success",
        timer: 3000
    });
}
function error(title, message) {
    swal({
        title: title,
        text: message,
        icon: "error",
        timer: 3000
    });
}



function getData(url, data, container) {
    $(container).html("<span class='fa fa-spin fa-spinner'></span>");
    $.ajax({
        url: url,
        method: "post",
        data: data,
        success: function (r) {
            $(container).html(r);
        }
    });
}

function sendData(url, data, $btn, modal, form, successInfo, callback) {
    $btn.button('loading');
    $.ajax({
        url: url,
        method: "post",
        data: data,
        success: function (r) {
            $btn.button('reset');
            if (r == "1") {
                $(modal).modal("hide");
                $(form).trigger("reset");
                success("Berhasil", successInfo);
                callback();
            } else {
                error("Kesalahan!", r);
                $btn.button('reset');
            }
        },
        eror: function (e) {
            $btn.button('reset');
            error("Kesalahan!", e.responseText);
        }
    });
}

function removeArrayItem(arr) {
    var what, a = arguments, L = a.length, ax;
    while (L > 1 && arr.length) {
        what = a[--L];
        while ((ax= arr.indexOf(what)) !== -1) {
            arr.splice(ax, 1);
        }
    }
    return arr;
}

