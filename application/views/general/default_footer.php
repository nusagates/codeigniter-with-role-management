 <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 0.0.1
    </div>
    <strong>Copyright &copy; 2014-2016 <a href="https://leap.id">TransLeap</a>.</strong> All rights
    reserved.
  </footer>
</div>
<!-- ./wrapper -->


<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<!-- Bootstrap 3.3.7 -->

<!-- AdminLTE App -->
<script src="<?php echo base_url() ?>/res/dist/js/adminlte.min.js"></script>
</body>
</html>
