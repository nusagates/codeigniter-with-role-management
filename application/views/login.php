<!DOCTYPE html>
<html>

    <head>
        <title>Mirambi Authentication Gate</title>
        <link rel="icon" href="<?php echo base_url() ?>/res/images/icon_mirambi.jpg" sizes="32x32" />
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP" crossorigin="anonymous">
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url() ?>/res/dist/js/sweetalert.min.js"></script>
        <script src="<?php echo base_url() ?>/res/dist/js/functions.js?<?php echo round(microtime(true) * 1000); ?>"></script>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <style>

            /* Coded with love by Mutiullah Samim */
            body,
            html {
                margin: 0;
                padding: 0;
                height: 100%;
                background:  cadetblue
            }
            .user_card {
                height: 400px;
                width: 350px;
                margin-top: auto;
                margin-bottom: auto;
                background: #fff;
                position: relative;
                display: flex;
                justify-content: center;
                flex-direction: column;
                padding: 10px;
                box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
                -webkit-box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
                -moz-box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
                border-radius: 5px;

            }
            .brand_logo_container {
                position: absolute;
                height: 170px;
                width: 170px;
                top: -75px;
                border-radius: 50%;
                background:  cadetblue;
                padding: 10px;
                text-align: center;
            }
            .brand_logo {
                height: 150px;
                width: 150px;
                border-radius: 50%;
                border: 2px solid #fe0000;
            }
            .form_container {
                margin-top: 100px;
            }
            .login_btn {
                width: 100%;
                background: #fe0000 !important;
                color: white !important;
            }
            .login_btn:focus {
                box-shadow: none !important;
                outline: 0px !important;
            }
            .login_container {
                padding: 0 2rem;
            }
            .input-group-text {
                background: #fe0000 !important;
                color: white !important;
                border: 0 !important;
                border-radius: 0.25rem 0 0 0.25rem !important;
            }
            .input_user,
            .input_pass:focus {
                box-shadow: none !important;
                outline: 0px !important;
            }
            .custom-checkbox .custom-control-input:checked~.custom-control-label::before {
                background-color: #c0392b !important;
            }

        </style>
    </head>
    <!--Coded with love by Mutiullah Samim-->
    <body>
        <div class="container h-100">
            <div class="d-flex justify-content-center h-100">
                <div class="user_card">
                    <div class="d-flex justify-content-center">
                        <div class="brand_logo_container">
                            <img src="<?php echo base_url() ?>/res/images/logo.jpg" class="brand_logo" alt="Logo">
                        </div>
                    </div>
                    <div class="d-flex justify-content-center form_container">
                        <form id="form-login">
                            <div class="input-group mb-3">
                                <div class="input-group-append">
                                    <span class="input-group-text"><i class="fas fa-envelope"></i></span>
                                </div>
                                <input type="email" name="email" class="form-control input_user" value="" placeholder="email">
                            </div>
                            <div class="input-group mb-2">
                                <div class="input-group-append">
                                    <span class="input-group-text"><i class="fas fa-key"></i></span>
                                </div>
                                <input type="password" name="password" class="form-control input_pass" value="" placeholder="password">
                            </div>
                        </form>
                    </div>
                    <div class="d-flex justify-content-center mt-3 login_container">
                        <button id="btn-login" type="button" name="button" class="btn login_btn">Login <span></span></button>
                    </div>
                    <div class="mt-4">
                        <div class="d-flex justify-content-center links">
                        </div>
                        <div class="d-flex justify-content-center links">
                            <a data-toggle="modal" data-target="#modal-reset-password" href="#">Lupa Password?</a>
                        </div>
                    </div>
                </div>
            </div>
            <div id="modal-reset-password" class="modal" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-sm" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Reset Password</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <p>Silahkan tulis alamat email Anda. Sebuah konfirmasi reset password akan dikirim ke email Anda.</p>
                            <div class="input-group mb-3">
                                <div class="input-group-append">
                                    <span class="input-group-text"><i class="fas fa-envelope"></i></span>
                                </div>
                                <input type="email" name="email" class="form-control input_user" value="" placeholder="email">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary">Reset Password</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            'use strict';
            var base_url = "<?php echo base_url() ?>/auth";
            var module_name = "<?php echo module_name($this) ?>";
            $(function () {
                $("#btn-login").click(function (e) {
                    e.preventDefault();
                    $("#btn-login").html("Memroses... <i class='fa fa-spin fa-spinner'> </i>");
                    var data = $("#form-login").serialize();
                    $.ajax({
                        url: base_url + "/login",
                        method: "post",
                        data: data,
                        success: function (r) {
                            if (r != 1) {
                                error("Kesalahan!", r);
                            } else {
                                success("Berhasil", "Verifikasi Berhasil. Mohon Tunggu.");
                                if (module_name != "auth") {
                                    setTimeout(function () {
                                        window.location.reload();
                                    }, 3000);
                                } else {
                                    setTimeout(function () {
                                        window.location.href=' <?php echo base_url() ?>';
                                    }, 3000);
                                }
                            }
                            $("#btn-login").html("login");
                        },
                        error: function () {
                            $("#btn-login").html("Login");
                        }
                    });
                });
            });
        </script>
    </body>
</html>
