<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-info">
                <div class="box-header">
                    <div class="box-title">Tambah</div>
                    <button data-toggle="modal" data-target="#modal-add" class="btn btn-sm btn-info pull-right"><i class="fa fa-plus"></i></button>
                </div>
                <div class="box-body">
                    <table id="table" class="display table table-bordered" style="width:100%">
                        <thead>
                            <tr>
                                <th>Nama</th>
                                <th width='10%'>Aksi</th>
                            </tr>
                        </thead> 
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="modal fade" id="modal-add">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Tambah</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="row">
                        <?php
                        html_input("Nama", "group_name", 12);
                        ?>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Batal</button>
                <button id="btn-add" type="button" class="btn btn-primary">Tambah</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<?php
modal("modal-edit", "sm", "Formulir Edit", "field-edit-container", "btn-edit", "Pebarui");
modal("modal-delete", "sm", "Peringatan", "field-delete-container", "btn-delete", "Hapus", "modal-warning", "btn-danger");
?>

</div>

<script>
    'use strict';
    var base_url = "<?php echo base_url() ?>/group";
    $(function () {
        var table = $('#table').DataTable({
            "ajax": base_url + "/get",
            "order": [[1, "asc"]],
            "columns": [
                {"data": "user_name"},
                {"data": "action"}
            ]
        });
        $("#btn-add").click(function () {
            var $btn = $(this);
            $btn.button('loading');
            var data = $("#modal-add form").serialize();
            $.ajax({
                url: base_url + "/add",
                method: "post",
                data: data,
                success: function (r) {
                    if (r != 1) {
                        error("Kesalahan!", r);
                    } else {
                        success("Berhasil", "Pengguna berhasil ditambahkan");
                        table.ajax.reload();
                        $("#modal-add").modal("hide");
                        $("#modal-add form").trigger("reset");
                    }
                    $btn.button('reset');
                },
                error: function () {
                    $btn.button('reset');
                }
            });
        });
        $("#table").on("click", ".btn-edit", function (e) {
            e.preventDefault();
            var data = {id: $(this).attr("data")};
            $("#modal-edit").modal("show");
            getData(base_url + "/edit", data, $("#modal-edit .field-edit-container"));
        });

        $("#btn-edit").click(function (e) {
            e.preventDefault();
            var data = $("#modal-edit form").serialize();
            var $btn = $(this);
            sendData(base_url + "/update", data, $btn, "#modal-edit", "#modal-edit form", "Pengguna berhasil diperbarui", table.ajax.reload);
        });
        $("#table").on("click", ".btn-delete", function (e) {
            e.preventDefault();
            var data = $(this).attr("data");
            var nama = $(this).attr("nama");
            $("#modal-delete").modal("show");
            $("#modal-delete .modal-body").html("Apakah Anda yakin ingin menghapus <b> " + nama + "</b>? <form><input type='hidden' name='id' value='" + data + "'/></form>");
        });
        $("#btn-delete").click(function () {
            var data = $("#modal-delete form").serialize();
            var $btn = $(this);
            sendData(base_url + "/delete", data, $btn, "#modal-delete", "", "Cabang berhasil dihapus.", table.ajax.reload);
        });
        $("#btn-delete-access").click(function () {
            var data = $("#modal-delete-access form").serialize();
            var $btn = $(this);
            sendData(base_url + "/deleteAccess", data, $btn, "#modal-delete-access", "", "Akses berhasil dihapus.", table_access.ajax.reload);
        });
        $("#btn-open-modal-link").click(function () {
            $("#modal-add-link").modal("show");
            getGroup();
            createLink("#tree");
        });
        $("#btn-open-modal-access").click(function () {
            $("#modal-add-access").modal("show");
            getAccessCode();
        });
        $("#table-link").on("click", ".btn-edit", function (e) {
            e.preventDefault();
            var data = {id: $(this).attr("data")};
            $("#modal-edit-link").modal("show");
            $("#modal-edit-link .modal-body").html("<div class='row'><div class='col-md-12'>Fitur ini belum befungsi</div></div>");
            //getData(base_url + "/editLink", data, $("#modal-edit-link .field-edit-container"));
        });
        $("#table-link").on("click", ".btn-delete", function (e) {
            e.preventDefault();
            var data = $(this).attr("data");
            $("#modal-delete-group").modal("show");
            $("#modal-delete-group .modal-body").html("Apakah Anda yakin ingin menghapus Akses Link <b> " + data + "</b>? <form><input type='hidden' name='id' value='" + data + "'/></form>");
        });

    });
</script>
