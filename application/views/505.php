
<!DOCTYPE html>
<html>
    <head>
        <link rel="icon" href="<?php echo base_url() ?>res/images/logo.jpg" sizes="32x32" />
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Not Authorized</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link rel="stylesheet" href="<?php echo base_url() ?>res/plugins/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>res/dist/css/AdminLTE.min.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>res/plugins/bootstrap/dist/css/bootstrap.min.css">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Google Font -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    </head>
    <body class="hold-transition lockscreen">
        <!-- Automatic element centering -->
        <div class="lockscreen-wrapper">
            <div class="lockscreen-logo">
                <a href="/"><b>Mirambi</b> Dashboard</a>
            </div>
            <!-- User name -->
            <div class="lockscreen-name"><?php echo $this->auth_model->user_name() ?></div>

            <!-- START LOCK SCREEN ITEM -->
            <div class="lockscreen-item">
                <!-- lockscreen image -->
                <div class="lockscreen-image">
                    <img src="<?php echo base_url() ?>res/dist/img/user2-160x160.jpg"  alt="User Image">
                </div>
                <!-- /.lockscreen-image -->

                <!-- lockscreen credentials (contains the form) -->
                <form class="lockscreen-credentials">
                    <div class="input-group">
                        <div class="row">
                            <div class="col-md-12">
                                Anda tidak memiliki hak untuk mengakses halaman ini.
                            </div>
                        </div>
                    </div>
                </form>
                <!-- /.lockscreen credentials -->

            </div>
            <!-- /.lockscreen-item -->
            <div class="text-center">
                <a href="<?php echo base_url() ?>/auth/logout">Gunakan akun yang lain</a>
            </div>
            <div class="lockscreen-footer text-center">
                Copyright &copy; 2014-2016 <a href="https://leap.id">TransLeap</a>.</strong> All rights
                reserved.
            </div>
        </div>
        <!-- /.center -->

        <script src="/res/plugins/jquery/dist/jquery.min.js"></script>
        <script src="/res/plugins/bootstrap/dist/js/bootstrap.min.js"></script>
    </body>
</html>
