<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * general_helper digunakan untuk mengelompokkan fungsi-fungsi umum yang digunakan dalam proyek
 * @link https://budairi.web.id Ahmad Budairi's Blog
 * @author Ahmad Budairi <nusagates@gmail.com>
 */
if (!function_exists('post')) {

    /**
     * Fungsi untuk mengambil nilai dari $_POST dan membuat variabel.
     * @example post("param", $var_param);
     * @param string $label parameter post yang diterima. Contoh $_POST['param'] bearti parameternya adalah param.
     * @param string $var variabel yang ingin digunakan untuk menyimpan nilai yang diambil dari parameter
     * @return string nilai dari $_POST['param']
     */
    function post($label, &$var = null) {
        return empty($_POST[$label]) || '' === ($var = trim($_POST[$label]));
    }

}

if (!function_exists("isUpdated")) {

    function isUpdated($db, $successMessage, $erroMessage) {
        if ($db->affected_rows() > 0) {
            exit($successMessage);
        } else {
            exit($erroMessage);
        }
    }

}
if (!function_exists("html_input")) {

    /**
     * Fungsi ini digunakan untuk membuat input HTML di dalam sebuah form
     * @param string $label Label atau keterangan input
     * @param string $id id dan name input
     * @param int $col nilai kolom bootstrap
     * @param string $value nilai default input
     * @param string $attr atribut tambahan untuk input contoh: <b>readonly='readonly'</b>
     * @param string $type tipe input seperti text, number, tel, dll.
     * @link https://budairi.web.id Ahmad Budairi's Blog
     */
    function html_input($label, $id, $col = '6', $value = "", $attr = '', $type = 'text') {
        ?>
        <div class="col-md-<?php echo $col ?>">
            <div class="form-group">
                <label for="<?php echo $id ?>"><?php echo $label ?></label>
                <input <?php echo $attr ?> value="<?php echo $value ?>" type="<?php echo $type ?>" class="form-control" name="<?php echo $id ?>" id="<?php echo $id ?>"/>
                <div id='msg'></div>
            </div>
        </div>
        <?php
    }

}

if (!function_exists("html_input_hidden")) {

    /**
     * Membuat input type hidden
     * @param string $id id dan name input
     * @param string $value nilai yang diberikan pada input
     */
    function html_input_hidden($id, $value) {
        ?>
        <input value="<?php echo $value ?>" type="hidden" name="<?php echo $id ?>" id="<?php echo $id ?>"/>
        <?php
    }

}

if (!function_exists("html_textarea")) {

    /**
     * Membuat textarea
     * @param string $label Nama atau label untuk ditampilkan di atas textarea
     * @param string $id id dan name untuk textarea
     * @param int $col nilai kolom bootstrap. diisi mulai dari 1 sampai 12.
     * @param string $value nilai default yang diberikan pada textarea
     */
    function html_textarea($label, $id, $col = '6', $value = '') {
        ?>
        <div class="col-md-<?php echo $col ?>">
            <div class="form-goup">
                <label for="<?php echo $id ?>"><?php echo $label ?></label>
                <textarea class="form-control" name="<?php echo $id ?>" id="<?php echo $id ?>"><?php echo $value ?></textarea>
            </div>
        </div>
        <?php
    }

}

if (!function_exists("modal")) {

    /**
     * Membuat block modal menggunakan bootstrap
     * @param string $modalId id modal
     * @param string $modalType tipe modal bisa diisi dengan md, sm, atau lg.
     * @param string $modalLabel Judul modal
     * @param string $modalFieldContainer id untuk kontainer yang ada di dalam tubuh modal (modal body)
     * @param string $modalBtnId ID tombol submit modal
     * @param string $modalBtnLabel label tombol submit modal
     * @param string $modalStyle gaya modal bisa diisi modal-primnary, modal-default, modal-success, dll
     * @param string $btnStyle gaya tombol submit bisa diisi btn-primnary, btn-default, btn-success, dll
     */
    function modal($modalId, $modalType, $modalLabel, $modalFieldContainer, $modalBtnId, $modalBtnLabel, $modalStyle = '', $btnStyle = 'btn-info') {
        ?>
        <div class="modal fade" id="<?php echo $modalId ?>">
            <div class="modal-dialog <?php echo $modalStyle ?> modal-<?php echo $modalType ?>">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"><?php echo $modalLabel ?></h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <form>
                                <div class="<?php echo $modalFieldContainer ?> col-md-12"></div>
                            </form>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Batal</button>
                        <button id="<?php echo $modalBtnId ?>" type="button" class="btn <?php echo $btnStyle ?>"><?php echo $modalBtnLabel ?></button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <?php
    }

}

if (!function_exists("html_select")) {

    /**
     * Membuat dom select pada html
     * @param string $label label ditampilkan di atas dom select
     * @param string $id id dan name untuk dom select
     * @param int $col nilai kolom bootstrap diisi 1 sampai 12.
     */
    function html_select($label, $id, $col = '6') {
        ?>
        <div class="col-md-<?php echo $col ?>">
            <div class="form-group">
                <label for="<?php echo $id ?>"><?php echo $label ?></label>
                <select class="form-control" id="<?php echo $id ?>" name="<?php echo $id ?>">
                    <option value="">Silahkan Pilih <?php echo $label ?></option>
                </select>
            </div>
        </div>
        <?php
    }

}

if (!function_exists("html_select_db")) {

    /**
     * Membuat dom select yang optionnya langsung diisi dari database
     * @param var $ctx variabel koneksi database. Contoh: $this->db
     * @param string $table nama tabel mysql
     * @param string $column kolom tabel mysql yang mau ditampilkan sebagai opsi (item option)
     * @param string $label label untuk dom select
     * @param string $id id dan name untuk dom select
     * @param int $col nilai kolom bootstrap diisi 1 sampai 12
     * @param string $defaultValue nilai defaut atau opsi yang terpilih. Jika nilainya sama dengan salah satu nilai opsi yang tersedia akan menambahkan atribut <b>selected</b> pada opsi tersebut.
     */
    function html_select_db($ctx, $table, $column, $label, $id, $col = '6', $defaultValue = '', $columnToDisplay = '') {
        $display = empty($columnToDisplay) ? $column : $columnToDisplay;
        ?>
        <div class="col-md-<?php echo $col ?>">
            <div class="form-group">
                <label for="<?php echo $id ?>"><?php echo $label ?></label>
                <select class="form-control" id="<?php echo $id ?>" name="<?php echo $id ?>">
                    <?php
                    $result = $ctx->query->select("*", "$table");
                    $data = $result["data"];
                    foreach ($data as $key => $value) {
                        $selected = $defaultValue == $value->$column ? "selected='selected'" : '';
                        echo "<option value='" . $value->$column . "' $selected>" . $value->$display . "</option>";
                    }
                    ?>
                </select>
            </div>
        </div>
        <?php
    }

}

if (!function_exists("module_name")) {

    /**
     * 
     * @param var $ctx diisi dengan variabel context. Contoh $this
     * @return string url segmen pertama setelah slash. Contoh <b>domain.com/login</b> maka nilai yang dihasilkan <b>login</b>
     */
    function module_name($ctx) {
        return $ctx->uri->segment(1);
    }

}

if (!function_exists("generate_validation")) {

    /**
     * Membuat validasi $_POST secara otomatis
     */
    function generate_validation() {
        foreach ($_POST as $key => $value) {
            echo 'if (post("' . $key . '", $' . $key . ')) {exit("' . ucwords(str_replace("_", " ", $key)) . ' harus diisi");}<br/>';
        }
        exit;
    }

}

if (!function_exists("generate_data")) {

    function generate_data() {
        $html = 'array(';
        foreach ($_POST as $key => $value) {
            $html .= "'$key' => $" . $key . ", <br/>";
        }
        $html .= ');';
        echo $html;
        exit;
    }

}