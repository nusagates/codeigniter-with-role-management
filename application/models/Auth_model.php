<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of User
 *
 * @author Ahmad Budairi <nusagates@gmail.com>
 */
class Auth_model extends CI_Model {

    function is_authorized($module_read, $load_template, $data) {
        if ($this->session->has_userdata('is_logged_in')) {
            if ($module_read == 1) {
                $this->load->view('general/default_header', $data);
                $this->load->view($load_template, $data);
                $this->load->view('general/default_footer');
            } else {
                $this->load->view('505');
            }
        } else {
            $this->load->view('login');
        }
    }

    function user_name() {
        return $data = $this->session->has_userdata('user_name') ? $this->session->userdata('user_name') : "";
    }

    function user_id() {
        return $data = $this->session->has_userdata('user_id') ? $this->session->userdata('user_id') : "";
    }

    function user_group() {
        return $this->query->getSingleValue("user_id", $this->user_id(), "users", "user_group");
    }

    function user_role($module) {
        $this->db->where("user_group", $this->user_group());
        $this->db->where("module_id", $module);
        $q = $this->db->get("user_module");
        if ($q->num_rows() > 0) {
            $r = $q->result();
            $data = array(
                "module_read" => $r[0]->module_read,
                "module_create" => $r[0]->module_create,
                "module_update" => $r[0]->module_update,
                "module_delete" => $r[0]->module_delete
            );
        } else {
            $data = array(
                "module_read" => -1,
                "module_create" => -1,
                "module_update" => -1,
                "module_delete" => -1
            );
            ;
        }
        return $data;
    }

}
