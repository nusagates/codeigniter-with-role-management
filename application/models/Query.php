<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Model_General
 *
 * @author nusag
 */
class Query extends CI_Model {

    function select($column, $table) {
        $query = $this->db->select("$column");
        $query->from("$table");
        //$query->order_by("level", "asc");
        $result = $query->get();
        if ($result->num_rows()) {
            return array("code" => 1, "data" => $result->result());
        } else {
            return array("code" => 0, "data" => "No Data");
        }
    }

    function insert($table, $columnName, $columnValue, $data) {
        $this->db->where($columnName, $columnValue);
        $q = $this->db->get($table);

        if ($q->num_rows() > 0) {
            exit("Kode pengguna " . $columnValue . " sudah digunakan. Mohon gunakan yang berbeda.");
        } else {
            if ($this->db->insert($table, $data) == TRUE) {
                exit("1");
            } else {
                exit("Gagal menambahkan data");
            }
        }
    }

    function update($table, $columnName, $columnValue, $data) {
        $this->db->where($columnName, $columnValue);
        $this->db->update($table, $data);
        exit("1");
    }

    function delete($table, $columnName, $columnValue) {
        $this->db->delete($table, array($columnName => $columnValue));
        exit("1");
    }

    function getAccess($accessGroup) {
        $query = $this->db->select("a.label, al.access_group");
        $query->from("access_link al");
        //$query->order_by("level", "asc");
        $query->where('access_group', $accessGroup);
        $query->join('akses a', 'a.access_code=al.access_code', 'left');
        $result = $query->get();
        $data = array();
        if ($result->num_rows()) {
            foreach ($result->result() as $value) {
                $data[] = $value->label;
            }
            return array("code" => 1, "data" => $data);
        } else {
            return array("code" => 0, "data" => "No Data");
        }
    }
    function getSingleValue($column_name, $column_value, $table, $column_to_display){
        $this->db->where($column_name, $column_value);
        $q = $this->db->get($table);
        if ($q->num_rows() > 0) {
            $data = $q->result();
            return $data[0]->$column_to_display;
        }
    }

}
