<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Branch
 *
 * @author Ahmad Budairi <nusagates@gmail.com>
 */
class Categories extends CI_Controller {

    private $table = 'letter_categories';
    private $primary_id = 'category_name';
    private $module_name = "categories";
    private $role;

    function __construct() {
        parent::__construct();
        $this->role = $this->auth_model->user_role($this->module_name);
    }

    public function index() {
        $data = array_merge(
                array(
                    "title" => "User Management"
                ),
                $this->role
        );
        $this->auth_model->is_authorized($data['module_read'], "categories", $data);
    }

    public function get() {
        $result = $this->query->select("*", $this->table);
        $data = $result["data"];
        if ($result['code'] == "1") {
            foreach ($data as $key => $value) {
                $btn_edit = "";
                $btn_delete = "";
                if ($this->role['module_update'] == 1) {
                    $btn_edit = "<a data='$value->category_name' class='btn-edit btn btn-sm btn-success' href=''><i class='fa fa-pencil-square'></i></a>";
                }
                if ($this->role['module_delete'] == 1) {
                    $btn_delete = "<a nama='$value->category_name' data='$value->category_name' class='btn-delete btn btn-sm btn-danger' href=''><i class='fa fa-times'></i></a>";
                }
                $obj[] = array(
                    "category_name" => $value->category_name,
                    "action" => "$btn_edit $btn_delete"
                );
            }
            echo json_encode(array("data" => $obj));
        } else {
            echo json_encode(array("data" => array()));
        }
    }

    public function add() {
        if (post("category_name", $category_name)) {
            exit("Grup Pengguna harus diisi");
        }
        $this->db->where($this->primary_id, $category_name);
        $q = $this->db->get($this->table);
        if ($q->num_rows() > 0) {
            exit("nama sudah digunakan. Silahkan gunakan nama yang lain.");
        }
        $this->db->insert($this->table, $_POST);
        if ($this->db->affected_rows() > 0) {
            exit("1");
        } else {
            exit("gagal menambahkan data ke database");
        }
    }

    public function edit() {
        if (post("id", $id)) {
            exit("ID tidak ditemukan");
        }
        $this->db->where($this->primary_id, $id);
        $q = $this->db->get($this->table);
        if ($q->num_rows() > 0) {
            $data = $q->result();
            html_input_hidden("id", $id);
            html_input("Nama kategori", "category_name", 12, $data[0]->category_name);
        } else {
            echo "Data $id tidak ditemukan dalam database";
        }
    }

    public function update() {
        if (post("id", $id)) {
            exit("ID tidak ditemukan");
        }
        if (post("category_name", $category_name)) {
            exit("Nama kategori harus diisi");
        }
        
        $data = array('category_name' => $category_name
        );
        $this->query->update($this->table, $this->primary_id, $id, $data);
    }

    public function delete() {
        if (post("id", $id))
            exit("ID tidak ditemukan");
        if ($id == $this->auth_model->user_id()) {
            exit("Anda tidak bisa menghapus diri sendiri.");
        }
        $this->query->delete("pengguna", $this->primary_id, $id);
    }

    public function tes() {
      echo  $this->query->getSingleValue("id", 1, "letter_template", "isi");
    }

}
