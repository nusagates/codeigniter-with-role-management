<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Branch
 *
 * @author Ahmad Budairi <nusagates@gmail.com>
 */
class Auth extends CI_Controller {

    private $table = 'users';
    private $primary_id = 'user_id';

    function __construct() {
        parent::__construct();
    }

    public function index() {
        $data = array(
            "title" => "Division Management"
        );
        $this->load->view('login', $data);
    }

    public function login() {
        if (post("email", $email)) {
            exit("Email harus diisi");
        }
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            exit("Format email tampaknya tidak valid.");
        }
        if (post("password", $password)) {
            exit("Password harus diisi");
        }
        $this->db->where("user_email", $email);
        $q = $this->db->get($this->table);
        if ($q->num_rows() > 0) {
            $data = $q->result();
            if(password_verify($password, $data[0]->user_password)){
                $session = array(
                    "is_logged_in"  => true,
                    "user_id"       => $data[0]->user_id,
                    "user_name"     => $data[0]->user_name,
                    "user_group"   => $data[0]->user_group
                );
                $this->session->set_userdata($session);
                exit("1");
            }else{
                exit("Password yang Anda masukkan tidak valid");
            }
        }else{
            exit("Data pengguna tidak ditemukan.");
        }
    }
    
    public function logout(){
        $array_items = array('is_logged_in', 'user_id', 'user_name', 'user_branch', 'user_group');
        $this->session->unset_userdata($array_items);
        header("location: ".base_url()."auth");
    }


   public function tes(){
       $this->load->view('505');
   }
}
