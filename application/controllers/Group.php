<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Branch
 *
 * @author Ahmad Budairi <nusagates@gmail.com>
 */
class Group extends CI_Controller {

    private $table = 'user_group';
    private $primary_id = 'group_id';

    function __construct() {
        parent::__construct();
    }

    public function index() {

        $data = array(
            "title" => "User Group Management"
        );
        $this->load->view('general/default_header', $data);
        $this->load->view("group", $data);
        $this->load->view('general/default_footer');
    }

    public function get() {
        $result = $this->query->select("*", $this->table);
        $data = $result["data"];
        if ($result['code'] == "1") {
            foreach ($data as $key => $value) {
                $btn_edit = "";
                $btn_delete = "";

                $btn_edit = "<a data='$value->group_id' class='btn-edit btn btn-sm btn-success' href=''><i class='fa fa-pencil-square'></i></a>";
                $btn_delete = "<a nama='$value->group_name' data='$value->group_id' class='btn-delete btn btn-sm btn-danger' href=''><i class='fa fa-times'></i></a>";
                $obj[] = array(
                    "user_name" => $value->group_name,
                    "action" => "$btn_edit $btn_delete"
                );
            }
            echo json_encode(array("data" => $obj));
        } else {
            echo json_encode(array("data" => array()));
        }
    }

    public function add() {
        if (post("group_name", $group_name)) {
            exit("Nama grup harus diisi");
        }
        $this->db->where("group_name", $group_name);
        $q = $this->db->get($this->table);
        if ($q->num_rows() > 0) {
            exit("Nama grup sudah digunakan. Silahkan pakai nama yang lain");
        }

        $this->db->insert($this->table, $_POST);
        if ($this->db->affected_rows() > 0) {
            exit("1");
        } else {
            exit("gagal menambahkan data ke database");
        }
    }

    public function edit() {
        if (post("id", $id)) {
            exit("ID tidak ditemukan");
        }
        $this->db->where($this->primary_id, $id);
        $q = $this->db->get($this->table);
        if ($q->num_rows() > 0) {
            $data = $q->result();
            html_input_hidden("id", $id);
            html_input("Nama grup", "group_name", 12, $data[0]->group_name, '', 'text');
        } else {
            echo "Data $id tidak ditemukan dalam database";
        }
    }

    public function update() {
        if (post("id", $id)) {
            exit("ID tidak ditemukan");
        }
        if (post("group_name", $group_name)) {
            exit("Nama grup harus diisi");
        }
        $this->db->where("group_name", $group_name);
        $this->db->where("group_id !=", $id);
        $q = $this->db->get($this->table);
        if ($q->num_rows() > 0) {
            exit("Nama grup sudah digunakan. Silahkan pakai nama yang lain");
        }
        $data = array(
            "group_name" => $group_name
        );
        $this->query->update($this->table, $this->primary_id, $id, $data);
    }

    public function delete() {
        if (post("id", $id))
            exit("ID tidak ditemukan");

        $this->query->delete($this->table, $this->primary_id, $id);
    }

}
