<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Branch
 *
 * @author Ahmad Budairi <nusagates@gmail.com>
 */
class User extends CI_Controller {

    private $table = 'users';
    private $primary_id = 'user_id';
    private $module_name = "user";
    private $role;

    function __construct() {
        parent::__construct();
        $this->role = $this->auth_model->user_role($this->module_name);
    }

    public function index() {
        $data = array_merge(
                array(
                    "title" => "User Management"
                ),
                $this->role
        );
        $this->auth_model->is_authorized($data['module_read'], "user", $data);
    }

    public function get() {
        $this->db->select('u.user_id, u.user_name, u.user_email, g.group_name user_group');
        $this->db->from('users u');
        $this->db->join('user_group g', 'u.user_group=g.group_id', 'left');
        $r = $this->db->get();
        if ($r->num_rows()) {
            $result = array("code" => 1, "data" => $r->result());
        } else {
            $result = array("code" => 0, "data" => "No Data");
        }
        $data = $result["data"];
        if ($result['code'] == "1") {
            foreach ($data as $key => $value) {
                $btn_edit = "";
                $btn_delete = "";
                if ($this->role['module_update'] == 1) {
                    $btn_edit = "<a data='$value->user_id' class='btn-edit btn btn-sm btn-success' href=''><i class='fa fa-pencil-square'></i></a>";
                }
                if ($this->role['module_delete'] == 1) {
                    $btn_delete = "<a nama='$value->user_name' data='$value->user_id' class='btn-delete btn btn-sm btn-danger' href=''><i class='fa fa-times'></i></a>";
                }
                $obj[] = array(
                    "user_name" => $value->user_name,
                    "user_email" => $value->user_email,
                    "user_group" => $value->user_group,
                    "action" => "$btn_edit $btn_delete"
                );
            }
            echo json_encode(array("data" => $obj));
        } else {
            echo json_encode(array("data" => array()));
        }
    }

    public function add() {
        if (post("user_group", $user_group)) {
            exit("Grup Pengguna harus diisi");
        }
        if (post("user_name", $user_name)) {
            exit("Nama Lengkap harus diisi");
        }
        if (post("user_email", $user_email)) {
            exit("Email harus diisi");
        }
        if (!filter_var($user_email, FILTER_VALIDATE_EMAIL)) {
            exit('Email Anda tampaknya tidak valid.');
        }
        if (post("user_password", $user_password)) {
            exit("Password harus diisi");
        }
        if (strlen($user_password) < 6) {
            exit("Panjang password minimal 6 karakter");
        }
        $this->db->where("user_email", $user_email);
        $q = $this->db->get($this->table);
        if ($q->num_rows() > 0) {
            exit("Email sudah digunakan. Silahkan gunakan email yang lain.");
        }

        $data = array('user_group' => $user_group,
            'user_name' => $user_name,
            'user_email' => $user_email,
            'user_password' => password_hash($user_password, PASSWORD_BCRYPT)
        );
        $this->db->insert($this->table, $data);
        if ($this->db->affected_rows() > 0) {
            exit("1");
        } else {
            exit("gagal menambahkan data ke database");
        }
    }

    public function edit() {
        if (post("id", $id)) {
            exit("ID tidak ditemukan");
        }
        $this->db->where($this->primary_id, $id);
        $q = $this->db->get($this->table);
        if ($q->num_rows() > 0) {
            $data = $q->result();
            html_input_hidden("id", $id);
            html_select_db($this, "user_group", "group_id", "Grup Pengguna", "user_group", 6, $data[0]->user_group, "group_name");
            html_input("Nama", "user_name", 6, $data[0]->user_name);
        } else {
            echo "Data $id tidak ditemukan dalam database";
        }
    }

    public function update() {
        if (post("id", $id)) {
            exit("ID tidak ditemukan");
        }
        if (post("user_group", $user_group)) {
            exit("Grup Pengguna harus diisi");
        }
        if (post("user_name", $user_name)) {
            exit("Nama Lengkap harus diisi");
        }
        $data = array('user_group' => $user_group,
            'user_name' => $user_name
        );
        $this->query->update($this->table, $this->primary_id, $id, $data);
    }

    public function delete() {
        if (post("id", $id))
            exit("ID tidak ditemukan");
        if ($id == $this->auth_model->user_id()) {
            exit("Anda tidak bisa menghapus diri sendiri.");
        }
        $this->query->delete("pengguna", $this->primary_id, $id);
    }

    public function tes() {
        $role = $this->auth_model->user_role($this->module_name);
        print_r($role);
        //echo $role['module_read'];
    }

}
