<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Branch
 *
 * @author Ahmad Budairi <nusagates@gmail.com>
 */
class Divisi extends CI_Controller {

    private $table = 'divisi';
    private $primary_id = 'kode_divisi';

    function __construct() {
        parent::__construct();

        $this->load->model('query');
    }

    public function index() {
        $data = array(
            "title" => "Division Management"
        );
        $this->load->view('general/default_header', $data);
        $this->load->view('divisi', $data);
        $this->load->view('general/default_footer');
    }

    public function get() {
        $this->db->select('d.kode_divisi, d.nama_divisi, c.nama nama_cabang, p.nama nama_pengguna, d.notes');
        $this->db->from('divisi d');
        $this->db->join('cabang c', 'd.kode_cabang=c.kode', 'left');
        $this->db->join('pengguna p', 'd.koordinator=p.id_pengguna', 'left');
        $r = $this->db->get();
        if ($r->num_rows()) {
            $result = array("code" => 1, "data" => $r->result());
        } else {
            $result = array("code" => 0, "data" => "No Data");
        }
        $data = $result["data"];
        if ($result['code'] == "1") {
            foreach ($data as $key => $value) {
                $obj[] = array(
                    "nama_divisi" => $value->nama_divisi,
                    "nama_cabang" => $value->nama_cabang,
                    "nama_pengguna" => $value->nama_pengguna,
                    "notes" => $value->notes,
                    "action" => "<a data='$value->kode_divisi' class='btn-edit btn btn-sm btn-success' href=''><i class='fa fa-pencil-square'></i></a> <a nama='$value->nama_divisi' data='$value->kode_divisi' class='btn-delete btn btn-sm btn-danger' href=''><i class='fa fa-times'></i></a>"
                );
            }
            echo json_encode(array("data" => $obj));
        } else {
            echo json_encode(array("data" => array()));
        }
    }

    public function add() {
        if (post("div_code", $div_code)) {
            exit("Kode Kode Divisi harus diisi");
        }
        if (post("div_name", $div_name)) {
            exit("Kode Nama Divisi harus diisi");
        }
        if (post("div_branch", $div_branch)) {
            exit("Silahkan Pilih Cabang");
        }
        if (post("div_coordinator", $div_coordinator)) {
            exit("Silahkan pilih Koordinator");
        }
        if (post("id_pengguna", $id_pengguna)) {
            exit("ID Koordinator Tidak Ditemukan");
        }
        post("div_notes", $div_notes);
        $this->db->where($this->primary_id, $div_code);
        $q = $this->db->get($this->table);

        if ($q->num_rows() > 0) {
            exit("Kode divisi " . $div_code . " sudah digunakan. Mohon gunakan kode yang lain.");
        } else {
            $data = array(
                "kode_divisi" => $div_code,
                "nama_divisi" => $div_name,
                "kode_cabang" => $div_branch,
                "koordinator" => $id_pengguna,
                "notes" => $div_notes
            );
            if ($this->db->insert($this->table, $data) == TRUE) {
                exit("1");
            } else {
                exit("Gagal menambahkan divisi baru");
            }
        }
    }

    public function edit() {
        if (post("id", $id))
            exit("ID tidak ditemukan");
        $this->db->where('kode_divisi', $id);
        $q = $this->db->get('divisi');
        if ($q->num_rows() > 0) {
            $data = $q->result();
            html_input_hidden("id", $id);
            html_input("Kode Divisi", "div_code", 6, $data[0]->kode_divisi, 'readonly');
            html_input("Nama Divisi", "div_name", 6, $data[0]->nama_divisi);
            html_select_db($this, "cabang", "kode", 'Cabang', "div_branch1", 6, $data[0]->kode_cabang);
            html_input("Koordinator", "div_coordinator1", 6, $this->query->getSingleValue("id_pengguna", $data[0]->koordinator, "pengguna", "nama"));
            html_input_hidden("id_pengguna1", $data[0]->koordinator);
            html_textarea("Catatan ", "div_notes", 12, $data[0]->notes);
            ?>
            <script>
                $("#div_coordinator1").autocomplete({
                    source: function (request, response) {
                        var cabang = $("#div_branch1").val();
                        var data = {term: request.term, cabang: cabang};
                        $.ajax({
                            url: base_url + "/getUser",
                            type: "POST",
                            data: data,
                            success: function (data) {
                                var d = jQuery.parseJSON(data);
                                response($.map(d, function (el) {
                                    return {
                                        label: el.nama + " | " + el.kode_cabang,
                                        nama: el.nama,
                                        id: el.id_pengguna
                                    };
                                }));
                            },
                            error: function () {
                                // $("#nama-penerima").removeClass("ui-autocomplete-loading");
                            }
                        });
                    },
                    select: function (event, ui) {
                        // Prevent value from being put in the input:
                        this.value = ui.item.nama;
                        $("#id_pengguna1").val(ui.item.id);
                        event.preventDefault();
                    }
                });
            </script>
            <?php

        } else {
            echo "Data $id tidak ditemukan dalam database";
        }
    }

    public function update() {
        if (post("id", $id)) {
            exit("Kode id harus diisi");
        }
        if (post("div_code", $div_code)) {
            exit("Kode divisi tidak ditemukan");
        }
        if (post("div_name", $div_name)) {
            exit("nama Divisi harus diisi");
        }
        if (post("div_branch1", $div_branch1)) {
            exit("Silahkan pilih cabang");
        }
        if (post("div_coordinator1", $div_coordinator1)) {
            exit("Koordinator harus diisi");
        }
        if (post("id_pengguna1", $id_pengguna1)) {
            exit("ID koordinator tidak ditemukan");
        }
        post("div_notes", $div_notes);
         $data = array(
                "kode_divisi" => $div_code,
                "nama_divisi" => $div_name,
                "kode_cabang" => $div_branch1,
                "koordinator" => $id_pengguna1,
                "notes" => $div_notes
            );
        $this->query->update($this->table, $this->primary_id, $id, $data);
    }

    public function delete() {
        if (post("id", $id))
            exit("ID tidak ditemukan");
        $this->query->delete($this->table, $this->primary_id, $id);
    }

    /**
     * 
     */
    public function getUser() {
        post("term", $term);
        post("cabang", $cabang);
        if (empty($term)) {
            $data[] = array(
                "nama" => "Tidak Ada Data",
                "id_pengguna" => "0",
                "kode_cabang" => "0"
            );
        } else {
            $this->db->select("nama, id_pengguna,kode_cabang");
            $this->db->where("kode_cabang", $cabang);
            $this->db->like('nama', $term);
            $q = $this->db->get('pengguna');
            if ($q->num_rows() > 0) {
                $result = $q->result();
                foreach ($result as $key => $value) {
                    $data[] = array(
                        "nama" => $value->nama,
                        "id_pengguna" => $value->id_pengguna,
                        "kode_cabang" => $value->kode_cabang
                    );
                }
            } else {
                $data[] = array(
                    "nama" => "Tidak Ada Data",
                    "id_pengguna" => "0",
                    "kode_cabang" => "0"
                );
            }
        }
        echo json_encode($data);
    }

}
